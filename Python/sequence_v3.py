from pyspcm import *
from spcm_tools import *
import sys
import math
import numpy as np
import time
from AWG import *
import msvcrt

awg = AWG()
awg.open()
awg.set_sampling_rate(MEGA(64))
awg.enable_channel(0)
awg.set_trigger(EXT0=SPC_TM_POS)
awg.set_sequence_mode(2)

seg_length = KILO(10)
fs = int32(0)
spcm_dwGetParam_i32(awg.card, SPC_MIINST_MAXADCVALUE, byref(fs))  # full scale value for data generation purpose
fs = fs.value
hs = fs // 2

x = np.linspace(0, seg_length, seg_length, dtype=int16)
seg0 = hs * np.sin(2*np.pi*seg_length*x)

cutoff = seg_length // 3
seg1 = np.zeros(seg_length, dtype=int16)
seg1[cutoff:cutoff*2] = hs

awg.write_segment(seg0, segment=0)
awg.write_segment(seg1, segment=1)
awg.configure_step(step=0, segment=0, nextstep=1, loop=10, condition=SPCSEQ_ENDLOOPONTRIG)
awg.configure_step(step=1, segment=1, nextstep=1, loop=10, condition=SPCSEQ_END)

while True:
    command = input("command: ")
    if command == 's':  # stop on s
        awg.stop()
        sys.stdout.write("stopping card\n")
        break
    elif command == 'c':
        awg.force_trigger()
    else:
        sys.stdout.write("nothing")
awg.close()


