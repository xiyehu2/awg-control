from pyspcm import *
from spcm_tools import *
import sys

#
# **************************************************************************
# Class for controlling the AWG
#
# User must install the AWG driver for Windows 8/9/10/11, found in
# https://spectrum-instrumentation.com/support/downloads.php
# Look under model M4i 6622-x8
#
# The python module provided in pyspcm is in essence a c/c++ wrapper,
# hence the use of ctype variables in this code.
# **************************************************************************
#


class AWG:
    def __init__(self):
        self.hCard = None  # holds the spcm card instance, this is used by all spcm calls
        self.cardType = int32(0)
        self.serialNumber = int32(0)
        self.sampleRate = int64(0)
        self.memSize = int64(0)
        self.fncType = int32(0)

    def open(self, remote=False):
        # check for remote connection
        if remote:
            self.hCard = spcm_hOpen(create_string_buffer(b'TCPIP::192.168.1.10::inst0::INSTR'))
        else:
            self.hCard = spcm_hOpen(create_string_buffer(b'/dev/spcm0'))
        if self.hCard is None:
            sys.stdout.write("no card found...\n")
        else:
            # initialize variables
            spcm_dwGetParam_i32(self.hCard, SPC_PCITYP,      byref(self.cardType))
            spcm_dwGetParam_i32(self.hCard, SPC_PCISERIALNO, byref(self.serialNumber))
            spcm_dwGetParam_i32(self.hCard, SPC_FNCTYPE,     byref(self.fncType))
            spcm_dwGetParam_i64(self.hCard, SPC_SAMPLERATE,  byref(self.sampleRate))
            spcm_dwGetParam_i64(self.hCard, SPC_PCIMEMSIZE,  byref(self.memSize))
            name = szTypeToName(self.cardType.value)
            sys.stdout.write("Found: {0} sn {1:05d}\n".format(name, self.serialNumber.value))
            sys.stdout.write("Sample Rate: {:.1f} MHz\n".format(self.sampleRate.value / 1000000))
            sys.stdout.write("Memory size: {:.0f} MBytes\n".format(self.memSize.value / 1024 / 1024))

    def close(self):
        spcm_vClose(self.hCard)

    # Check if an error has occurred, return 1 if true
    def checkError(self, customMsg=None):
        toPrint = 0
        msg = "Checking error at " + customMsg + " ... "
        if customMsg is not None:
            toPrint = 1
            sys.stdout.write(msg)
        errReg = uint32(0)
        errVal = int32(0)
        errText = ''
        errCode = spcm_dwGetErrorInfo_i32(self.hCard, byref(errReg), byref(errVal), errText)
        if errCode:
            if toPrint:
                sys.stdout.write(errText)
            else:
                sys.stdout.write(msg + errText)
            return 1
        elif toPrint:
            sys.stdout.write("no error\n")
        return 0
