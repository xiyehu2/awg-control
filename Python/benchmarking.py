import numpy as np

from waveform import *
from cupyx.profiler import benchmark
import cupy as cp


def test(f_idx, t_idx):
    paths, off = get_rearrange_paths(f_idx, t_idx)
    create_moving_array_reduced(_sig, _table_cp, paths, off)


def count(paths, path_table, off):
    counter = 0
    for i, j in paths:
        if i == j:
            continue
        if (i, j) in path_table:
            counter += 1
        # else:
        #     print((i, j), "not in path table")
    for i in off:
        counter += 1
    return counter


data = np.load("data/table-half_31.npz", allow_pickle=True)
table = data['path_table'].item()
twzr = data['wfm'].item()
static_sig = data['static_sig']
target = data['target']
t_idx = np.nonzero(target)[0]
nt = twzr.omega.size

_table_cp = {}

for key in table:
    _table_cp[key] = cp.array(table[key])

n_repeat = 500
_sig = cp.array(static_sig)
times = np.zeros((nt+1,2))
filling_ratio = np.zeros((nt+1,2))
n_move = np.zeros((nt+1,n_repeat))

for i in range(nt+1):
    f_prob = i/nt
    calc_t = np.zeros(n_repeat)
    ratio = np.zeros(n_repeat)
    nm = np.zeros(n_repeat)
    print(i, f_prob)
    for j in range(n_repeat):
        filled = np.random.rand(nt)
        tr = filled < f_prob
        fa = filled >= f_prob
        filled[tr] = 1
        filled[fa] = 0
        f_idx = np.nonzero(filled)[0]
        b = benchmark(
            test,
            (f_idx, t_idx),
            n_repeat=1
        )
        # stuff to save
        ratio[j] = f_idx.size / nt
        calc_t[j] = b.gpu_times + b.cpu_times
        paths, off = get_rearrange_paths(f_idx, t_idx)
        nm[j] = count(paths, _table_cp, off)

    # n_move[i,0] = np.mean(nm)
    # n_move[i,1] = np.var(nm)
    n_move[i] = nm
    times[i,0] = np.mean(calc_t)
    times[i,1] = np.var(calc_t)
    filling_ratio[i,0] = np.mean(ratio)
    filling_ratio[i,1] = np.var(ratio)

np.savez(
    f"data/reduced-benchmark_{nt}-half.npz",
    wfm=twzr,
    target=target,
    filling_ratio=filling_ratio,
    times=times,
    n_move=n_move
)

