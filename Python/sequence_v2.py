#
# **************************************************************************
# AWG fast sequence control V2
#
# Must install the AWG driver for Windows 8/9/10/11, found in
# https://spectrum-instrumentation.com/support/downloads.php
# Look under model M4i 6622-x8
#
# The python module provided is in essence a c/c++ wrapper,
# hence the extensive use of ctype variables and c-like coding style in this code.
# **************************************************************************
#
# This code makes AWG play one waveform on repeat until a trigger event.
# Upon a triggering event the AWG will play a second waveform on repeat.
#
# **************************************************************************


from pyspcm import *
from spcm_tools import *
import sys
import math
import numpy as np
import time
from AWG_outdated import *
import msvcrt


def errorExit(awg, msg=None):
    """
    helper func to exit the program when an error is detected
    :param awg: awg instance
    :param msg: optional msg for testing purpose
    """
    if awg.checkError(msg):
        awg.close()
        exit(1)


def configureStep(card, step, segment, loop, next, condition):
    """
    helper to configure a step in the sequence
    :param awg: awg instance
    :param step: step in the sequence to configure, starts at SPC_SEQMODE_STEPMEM0
    :param segment: memory segment this step is linked to
    :param loop: number of times this step is repeated
    :param next: next step to be played after this step
    :param condition: see usage of this function for detail
    """
    mask = (condition << 32) | (loop << 32) | (next << 16) | segment  # 64-bit mask
    spcm_dwSetParam_i64(card, SPC_SEQMODE_STEPMEM0 + step, mask)


# helper to write data onto a memory segment
def writeSeg(card, dataptr, segment, size, numCh):
    spcm_dwSetParam_i32(card, SPC_SEQMODE_WRITESEGMENT, segment)  # set current segment
    spcm_dwSetParam_i32(card, SPC_SEQMODE_SEGMENTSIZE,  size)  # set segment size
    # transfer data
    segLength = size * 2 * numCh
    spcm_dwDefTransfer_i64(card, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, int32(0), dataptr, int64(0), segLength)
    spcm_dwSetParam_i32(card, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA)


awg = AWG()
awg.open()
errorExit(awg, "start up")
hCard = awg.hCard

# **************************************************************************
# mode initialization
# **************************************************************************
lChEnable = int64(CHANNEL0)
# lMemSamples = int64(KILO_B(64))
lNumSegments = 2  # number of sequences the memory is divided into, must be powers of 2: 2,4,8...
spcm_dwSetParam_i32(hCard, SPC_CARDMODE,   SPC_REP_STD_SEQUENCE)  # Sequence replay mode
spcm_dwSetParam_i32(hCard, SPC_SEQMODE_MAXSEGMENTS, lNumSegments)  # set number of sequences the memory is divided into
spcm_dwSetParam_i32(hCard, SPC_SEQMODE_STARTSTEP,   0)  # set step 0 as the starting step
spcm_dwSetParam_i64(hCard, SPC_SAMPLERATE, MEGA(64))  # set 64 MHz sampling rate
spcm_dwSetParam_i64(hCard, SPC_CHENABLE,   lChEnable)  # enable channel 0
# spcm_dwSetParam_i64(hCard, SPC_MEMSIZE,    lMemSamples)  # MEMSIZE is not used in sequence replay mode
spcm_dwSetParam_i32(hCard, SPC_CLOCKOUT,   0)  # no clock output
errorExit(awg, "mode init")

# **************************************************************************
# set up trigger mode
# **************************************************************************
spcm_dwSetParam_i32(hCard, SPC_TRIG_ORMASK,      SPC_TMASK_EXT0) # using external channel 0 as trigger
# spcm_dwSetParam_i32(hCard, SPC_TRIG_ORMASK,      SPC_TMASK_NONE)
spcm_dwSetParam_i32(hCard, SPC_TRIG_ANDMASK,     0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ORMASK0,  0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ORMASK1,  0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ANDMASK0, 0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ANDMASK1, 0)
spcm_dwSetParam_i32(hCard, SPC_TRIGGEROUT,       0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_EXT0_MODE,   SPC_TM_POS)  # positive edge trigger
errorExit(awg, "trigger")

# **************************************************************************
# set up channel modes
# **************************************************************************
lNumChannels = int32(0)
spcm_dwGetParam_i32(hCard, SPC_CHCOUNT, byref(lNumChannels))
for lCh in range (0, lNumChannels.value, 1):
    # second entry looks weird because the mask for having multiple channels is "interleaved", see data organization chapter in manual for more detail
    spcm_dwSetParam_i32 (hCard, SPC_ENABLEOUT0    + lCh * (SPC_ENABLEOUT1    - SPC_ENABLEOUT0),    1)  # enable channel
    spcm_dwSetParam_i32 (hCard, SPC_AMP0          + lCh * (SPC_AMP1          - SPC_AMP0),          1000)  # set output amplitude
    spcm_dwSetParam_i32 (hCard, SPC_CH0_STOPLEVEL + lCh * (SPC_CH1_STOPLEVEL - SPC_CH0_STOPLEVEL), SPCM_STOPLVL_ZERO) #
errorExit(awg, "channel init")

# **************************************************************************
# do data calculation and set up segments/memory
# **************************************************************************
dwFactor = 6  # weird factor related to the minimum size of a segment, not sure if necessary.
segLength = KILO(1)  # 1k samples
fullScale = int32(0)
spcm_dwGetParam_i32(hCard, SPC_MIINST_MAXADCVALUE, byref (fullScale))  # full scale value for data generation purpose
fullScale = fullScale.value
halfScale = fullScale // 2

# generate segment 0
combinedData = np.zeros(dwFactor*segLength*lNumChannels.value, dtype=int16)  # this array will contain all generated data
for i in range(lNumChannels.value):
    x = np.linspace(0, segLength, segLength)
    data = halfScale * np.sin(2*np.pi*segLength*x)  # sine wave with a period of segLength
    combinedData[i::2] = data.astype(int16)  # output same data onto all channels
dataptr = combinedData.ctypes.data_as(POINTER(int16))  # convert data into a c-like array, with dataptr pointing to the address of first element
# write segment data
writeSeg(hCard, dataptr=dataptr, segment=0, size=segLength, numCh=lNumChannels)

# generate segment 1
combinedData = np.zeros(dwFactor*segLength*lNumChannels.value, dtype=int16)  # this array will contain all generated data
for i in range(lNumChannels.value):
    data = np.zeros(segLength, dtype=int16)
    oneThirdPoint = segLength // 3
    data[oneThirdPoint:oneThirdPoint*2] = halfScale  # square wave in the middle
    combinedData[i::2] = data.astype(int16)  # output same data onto all channels
dataptr = combinedData.ctypes.data_as(POINTER(int16))  # convert data into a c-like array, with dataptr pointing to the address of first element
# write segment data
writeSeg(hCard, dataptr, 1, segLength, lNumChannels)

# **************************************************************************
# configure steps
# **************************************************************************
configureStep(hCard, step=0, segment=0, loop=1, next=1, condition=SPCSEQ_ENDLOOPONTRIG)
configureStep(hCard, step=1, segment=1, loop=1, next=0, condition=SPCSEQ_ENDLOOPONTRIG)

# **************************************************************************
# running the sequence
# **************************************************************************
spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_ENABLETRIGGER)  # start the card, enable trigger and wait for trigger to start
while True:
    command = input("command: ")
    if command == 's':  # stop on s
        spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_CARD_STOP)
        sys.stdout.write("stopping card\n")
        break
    elif command == 'c':
        spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_CARD_FORCETRIGGER)
        errorExit(awg, "force trigger")
    else:
        sys.stdout.write("nothing")
awg.close()



