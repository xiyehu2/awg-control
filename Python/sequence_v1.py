#
# **************************************************************************
# AWG fast sequence control V1
#
# Must install the AWG driver for Windows 8/9/10/11, found in
# https://spectrum-instrumentation.com/support/downloads.php
# Look under model M4i 6622-x8
#
# The python module provided is in essence a c/c++ wrapper,
# hence the extensive use of ctype variables and c-like coding style in this code.
# **************************************************************************
#
# This code makes AWG play one iteration of the written waveform upon a hardware trigger from channel ext0,
# or a 'c' keyboard press. A 's' keyboard press stops the AWG.
#
# **************************************************************************
#
from pyspcm import *
from spcm_tools import *
import sys
import math
import numpy as np
from time import sleep
from AWG_outdated import *
import msvcrt


# helper func to exit the program when an error is detected, also takes a string for testing purpose
def errorExit(awg, msg):
    if awg.checkError(msg):
        awg.close()
        exit(1)


awg = AWG()
awg.open()
errorExit(awg, "start up")
hCard = awg.hCard

spcm_dwSetParam_i32(hCard, SPC_CLOCKOUT, 0)

lChEnable = int64(CHANNEL0)
lMemSamples = int64(KILO_B(64))

spcm_dwSetParam_i32(hCard, SPC_CARDMODE, SPC_REP_STD_SINGLERESTART)  # single restart mode
spcm_dwSetParam_i64(hCard, SPC_SAMPLERATE, MEGA(64))  # set 64 MHz sampling rate
spcm_dwSetParam_i64(hCard, SPC_CHENABLE, lChEnable)  # enable channel 0
spcm_dwSetParam_i64(hCard, SPC_MEMSIZE, lMemSamples)  # set memory size of a single waveform to 16 kB
spcm_dwSetParam_i32(hCard, SPC_LOOPS, 0)  # run # of times, or 0 for endless loop until stopped by trigger
errorExit(awg, "mode init")

# set up the trigger mode
spcm_dwSetParam_i32(hCard, SPC_TRIG_ORMASK,      SPC_TMASK_SOFTWARE) # using external channel 0 as trigger
# spcm_dwSetParam_i32(hCard, SPC_TRIG_ORMASK,      SPC_TMASK_NONE)
spcm_dwSetParam_i32(hCard, SPC_TRIG_ANDMASK,     0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ORMASK0,  0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ORMASK1,  0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ANDMASK0, 0)
spcm_dwSetParam_i32(hCard, SPC_TRIG_CH_ANDMASK1, 0)
spcm_dwSetParam_i32(hCard, SPC_TRIGGEROUT,       0)
# spcm_dwSetParam_i32(hCard, SPC_TRIG_EXT0_MODE,   SPC_TM_POS)  # positive edge trigger
errorExit(awg, "trigger")

# set up the channels
lNumChannels = int32(0)
spcm_dwGetParam_i32(hCard, SPC_CHCOUNT, byref(lNumChannels))
for lCh in range (0, lNumChannels.value, 1):
    spcm_dwSetParam_i32 (hCard, SPC_ENABLEOUT0    + lCh * (SPC_ENABLEOUT1    - SPC_ENABLEOUT0),    1)
    spcm_dwSetParam_i32 (hCard, SPC_AMP0          + lCh * (SPC_AMP1          - SPC_AMP0),          1000)
    spcm_dwSetParam_i32 (hCard, SPC_CH0_STOPLEVEL + lCh * (SPC_CH1_STOPLEVEL - SPC_CH0_STOPLEVEL), SPCM_STOPLVL_ZERO)

# buffer loading preparation
lBytesPerSample = int32(0)
spcm_dwGetParam_i32(hCard, SPC_MIINST_BYTESPERSAMPLE, byref(lBytesPerSample))
# use continuous mem whenever possible
swBufferSize = uint64(lMemSamples.value * lBytesPerSample.value * lNumChannels.value)
pvBuffer = c_void_p()
swContBufLen = uint64(0)
spcm_dwGetContBuf_i64(hCard, SPCM_BUF_DATA, byref(pvBuffer), byref(swContBufLen))
sys.stdout.write("Continuous buffer length: {:d}\n".format(swContBufLen.value))
if swContBufLen.value > swBufferSize.value:
    sys.stdout.write("Using continuous buffer\n")
else:
    pvBuffer = pvAllocMemPageAligned(swBufferSize.value)
    sys.stdout.write("Using manually allocated buffer\n")
errorExit(awg, "buffer prep")

# waveform calculation
wData = np.linspace(0, lMemSamples.value, lMemSamples.value, dtype=int16)  # a ramping signal
wDataPtr = wData.ctypes.data_as(POINTER(int16))  # buffer writing function requires a pointer to the start of the array
pnBuffer = cast(pvBuffer, ptr16)
for i in range(0, lMemSamples.value, 1):  # a ramping signal
    pnBuffer[i] = i
spcm_dwDefTransfer_i64(hCard, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, int32(0), wDataPtr, uint64(0), swBufferSize)
spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA)
errorExit(awg, "data writing")

spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_ENABLETRIGGER)  # start the card, enable trigger and wait for trigger to start
errorExit(awg, "starting card")

lCardStatus = int32(0)
while True:
    lKey = input("command: ")
    if lKey == 's':  # stop on s
        spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_CARD_STOP)
        sys.stdout.write("stopping card\n")
        break
    elif lKey == 'c':
        spcm_dwSetParam_i32(hCard, SPC_M2CMD, M2CMD_CARD_FORCETRIGGER)
        errorExit(awg, "force trigger")
    else:
        sleep(1)
        print("nothing")

    # spcm_dwGetParam_i32(hCard, SPC_M2STATUS, lCardStatus)
    # if lCardStatus.value & M2STAT_CARD_READY != 0:
    #     break

awg.close()

