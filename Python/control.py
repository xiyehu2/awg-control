from AWG import *
import code
import readline
import rlcompleter


def trigger():
    global awg
    awg.force_trigger()
    print("forcing a trigger...")


def stop():
    global awg
    awg.stop()
    print("stopping awg output...")


def start():
    global awg
    awg.run()
    awg.force_trigger()
    print("starting awg output...")


def close():
    global awg
    awg.close()
    print("closing awg and quitting...")
    exit(0)

# load waveform data
wfm_data = np.load("data/single.npz", allow_pickle=True)
static_sig = wfm_data['signal']
empty = np.zeros(static_sig.shape)
wfm = wfm_data['wfm'].item()
sampling_rate = wfm.sample_rate

# AWG stuff
awg = AWG()
awg.open(id=b'/dev/spcm1')  # change this to b'/dev/spcm0' for top AWG
awg.set_sampling_rate(sampling_rate)
awg.toggle_channel(0, amplitude=2500)
awg.set_trigger(EXT0=SPC_TM_POS)
awg.set_sequence_mode(2)
awg.write_segment(static_sig, segment=0)
awg.write_segment(empty, segment=1)
awg.configure_step(step=0, segment=0, nextstep=1, loop=1, condition=SPCSEQ_ENDLOOPONTRIG)
awg.configure_step(step=1, segment=1, nextstep=0, loop=1, condition=SPCSEQ_ENDLOOPONTRIG)

# console
vars = globals()
vars.update(locals())
readline.set_completer(rlcompleter.Completer(vars).complete)
readline.parse_and_bind("tab: complete")
code.InteractiveConsole(vars).interact()

