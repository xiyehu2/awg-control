import numpy as np

from waveform import *
from waveform_plots import *

np.random.seed(0)
cf = MEGA(105)  # center frequency
df = MEGA(2.5)  # delta frequency
n = 2  # number of tones to generate in either direction of cf, total number of tweezer is 2*n+1
nt = 2*n+1  # total number of tweezers
fr = KILO(10)
# # sampling_rate and sample_len must follow some relationship explained in the wiki page.
sampling_rate = int(614.4e6)  # sampling rate
# sample_len = 512 * 600  # sample_len
twzr = Waveform(cf, df, n, sampling_rate, fr)

# amplitude uniformity adjustments, just make sure its called amps at the end
# scale = 2**12
# ampMax = scale/np.sqrt(nt)
# amps = ampMax * np.ones(nt)
# corr = np.array([1.53312825, 1.35073669, 1.252971, 1.06263741, 1.])
# amps *= np.sqrt(corr)

# phase adjustments
# phase = np.load("array_phase.npz")['phase'][:nt]  # phase table from Caltech

# twzr.amplitude = amps
# twzr.phi = phase
# static_sig = create_static_array(twzr, sample_len)
# empty = np.zeros(sample_len)
# table = create_path_table(twzr, save=True)
# data = np.load("data/rearrange_table_11.npz", allow_pickle=True)
# table = data['path_table']
# twzr = data['wfm'].item()
# target = np.array([1,1,1,1,0,0,0,0,0,0,0])
# filled = np.array([1,1,0,0,1,0,1,1,1,0,0])
# target = np.array([1,0,1,1,0])
# filled = np.array([1,1,0,1,1])
# target = np.nonzero(target)[0]
# filled = np.nonzero(filled)[0]
# path = get_rearrange_paths(target, filled)

target = np.array([1,1,1,0,0])
filled = np.array([1,0,0,1,1])
t_idx = np.nonzero(target)[0]
f_idx = np.nonzero(filled)[0]
table, sig = create_path_table_reduced(twzr, t_idx)
# np.savez(
#     "data/reduced_path_table.npz",
#     path_table=table,
#     static_sig=sig,
#     wfm=twzr,
#     target=target
# )
# data = np.load("data/reduced_path_table_5.npz", allow_pickle=True)
# table = data['path_table'].item()
# sig = data['static_sig']
# twzr = data['wfm'].item()
# target = data['target']
paths, off = get_rearrange_paths(f_idx, t_idx)
# #################################
# # end = 1930345  # dw = 1
# # end = 2730325  # dw = 2
# # end = 2729906  # dw = 2'
# end = 3343227  # dw = 3
# # end = 3343612  # dw = 3'
# # end = 3860640  # dw = 4
# # move = table[(1,0)] % (np.pi)
# # static = table[(0,0)] % (np.pi)
# move = np.sin(table[(3,0)])
# static = np.sin(table[(0,0)])
# r = 20
# m = np.arcsin(move[end-r:end+r])
# # s = np.arcsin(static[end-r:end+r])
# s = np.zeros(r*2)
# s[r:] = static[:r]
# # m = move[end-r:end+r]
# # s = static[end-r:end+r]
# # diff = move[end-10:end+10]-static[end-10:end+10]
# import matplotlib.pyplot as plt
# x = np.arange(20)
# plt.figure()
# plt.plot(m, '.-', label='move')
# plt.plot(s, '.-', label='static')
# plt.legend()
# plt.show()
#########################
create_moving_array_reduced(sig, table, paths, off)
fname = "data/reduced_move_sig.npz"
np.savez(fname, signal=sig, wfm=twzr)
plot_main(fname)
