from waveform import *

np.random.seed(0)

# parameters to play around 
# ----------------------------------------------------------------------------
cf = MEGA(105)  # center frequency
df = MEGA(2.5)  # delta frequency
n = 2  # number of tones to generate in either direction of cf, total number of tweezer is 2*n+1
nt = 2*n+1  # total number of tweezers
# sampling_rate and sample_len must follow some relationship explained in the wiki page.
sampling_rate = int(614.4e6)  # sampling rate
sample_len = 512 * 600  # sample_len

# amplitude uniformity adjustments, just make sure its called amps at the end
scale = 2**11
ampMax = scale/np.sqrt(nt)
amps = ampMax * np.ones(nt)
corr = np.array([1.53312825, 1.35073669, 1.252971, 1.06263741, 1.])
amps *= np.sqrt(corr)

# phase adjustments
phase = np.load("array_phase.npz")['phase'][:nt]  # phase table from Caltech
# ----------------------------------------------------------------------------

# MAGIC DO NOT CHANGE. one day i will understand, maybe
# ----------------------------------------------------------------------------
pvAllocMemPageAligned(sample_len * 2)
# ----------------------------------------------------------------------------

# setting up awg sequence, explained in wiki
# ----------------------------------------------------------------------------
awg = AWG()
awg.open(id=b'/dev/spcm1')  # change this to b'/dev/spcm0' for top AWG
awg.set_sampling_rate(sampling_rate)
awg.toggle_channel(0, amplitude=2500)
awg.set_trigger(EXT0=SPC_TM_POS)
awg.set_sequence_mode(2)

twzr = Waveform(cf, df, n, sampling_rate)
twzr.amplitude = amps
twzr.phi = phase
static_sig = create_static_array(twzr, sample_len)
empty = np.zeros(sample_len)

awg.write_segment(static_sig, segment=0)
awg.write_segment(empty, segment=1)
awg.configure_step(step=0, segment=0, nextstep=1, loop=1, condition=SPCSEQ_ENDLOOPONTRIG)
awg.configure_step(step=1, segment=1, nextstep=0, loop=1, condition=SPCSEQ_ENDLOOPONTRIG)
# ----------------------------------------------------------------------------

# running the awg
# ----------------------------------------------------------------------------
awg.run()
awg.force_trigger() # this is equivalement to an actual hardware trigger
print("Outputing...")
while True:
    command = input("enter c to stop, s to switch sequence step: ")
    if command == 'c':
        awg.stop()
        print("stopping")
        break
    elif command == 's':
        awg.force_trigger()
        print("switching sequence step")
awg.stop()
# ----------------------------------------------------------------------------

awg.close()
