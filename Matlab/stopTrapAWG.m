function stopTrapAWG(awg_trap)
    % If you try and "stop" when the card wasn't actually running it will
    % hard crash MATLAB
    
    % helper maps to use label names for registers
    mRegs = spcMCreateRegMap();

    error = spcm_dwSetParam_i32(awg_trap.cardInfo.hDrv, mRegs('SPC_M2CMD'), mRegs('M2CMD_CARD_STOP'));
    fprintf(' ...................... replay done\n');

    % ***** close card *****
    spcMCloseCard(awg_trap.cardInfo);
    disp('AWG stopped');

end