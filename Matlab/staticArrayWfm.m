function [wfm] = staticArrayWfm(numTweezers, f0, df, ampScaling)
    %STATICARRAYWFM Summary of this function goes here
    %   Detailed explanation goes here

    % Pretty sure this only works for odd numbers of arrays right now, but
    % simple to make it work for even as well
    
    wfm = struct();
    wfm.amp = ampScaling * ones(1, numTweezers);


    wfm.freq = f0 + df * ((1:numTweezers) - numTweezers / 2 - 1 / 2);

    wfm.freq = wfm.freq;

    % This is basically just a random guess of the phases. Ultimately you
    % want to optimize the phases by minimizing mixing into sum and
    % difference fourier modes, so this really is just a first
    % approximation that kinda works.
    phaseStruct = load('arrayPhase.mat');
    phase = phaseStruct.arrayPhase;

    if length(phase) < length(wfm.freq)
        phase = repmat(phase, [1, 2]);
    end
    % disp('setting phases to 0!')
    wfm.phase = phase(1:length(wfm.freq));

end
