function [awg_trap, success] = updateTrapAWG(wfms, awg_trap)

    cardInfo = awg_trap.cardInfo;
    memSamples = awg_trap.memSamples;

    % helper maps to use label names for registers
    mRegs = spcMCreateRegMap();

    %Get currentStep
    [~, currentStep] = spcm_dwGetParam_i32 (cardInfo.hDrv, 349950);  % 349950 = SPC_SEQMODE_STATS

    if currentStep ~= 0
       fprintf('Current step in memory is not 0! Some things might go wrong!\n') 
    end


    % compute signal
    signals = [];
    overPowerFlag = false;
    for ind = 1:cardInfo.maxChannels
        if awg_trap.active_channels(ind)
            wfm = wfms.(sprintf('ch%i', ind - 1));
            signal = staticArraySignal(wfm, awg_trap);
        else
            signal = zeros(1, memSamples);
        end
        
        signals = [signals; signal];
    end

    %Update the current step
    default_memory_segment = 0;

    error = spcm_dwSetParam_i32(cardInfo.hDrv, mRegs('SPC_SEQMODE_WRITESEGMENT'), default_memory_segment);
    error = spcm_dwSetParam_i32(cardInfo.hDrv, mRegs('SPC_SEQMODE_SEGMENTSIZE'), memSamples);
    errorCode = spcm_dwSetData(cardInfo.hDrv, 0, memSamples, numel(awg_trap.active_channels),...
        0, signals(1, :), signals(2, :), signals(3, :), signals(4, :));


    spcMSetupSequenceStep(cardInfo, currentStep, currentStep + 1, 0, 1, 0);
    spcMSetupSequenceStep(cardInfo, currentStep + 1, 0, default_memory_segment, 1, 0);
    spcMSetupSequenceStep(cardInfo, 0, 1, default_memory_segment, 1, 1);

    awg_trap.cardInfo = cardInfo;
    if ~overPowerFlag
        success = true;
    else
       success = false; 
    end

end
